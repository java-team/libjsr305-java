Source: libjsr305-java
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: James Page <james.page@ubuntu.com>
Section: java
Priority: optional
Build-Depends: debhelper-compat (= 13),
               default-jdk-headless,
               default-jdk-doc,
               javahelper,
               maven-repo-helper
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/java-team/libjsr305-java
Vcs-Git: https://salsa.debian.org/java-team/libjsr305-java.git
Homepage: http://code.google.com/p/jsr-305/

Package: libjsr305-java
Architecture: all
Depends: ${java:Depends},
         ${misc:Depends}
Suggests: libjsr305-java-doc
Description: Java library that provides annotations for software defect detection
 This library provides the implementation of Java Specification Request 305.
 JSR-305 specifies annotations for software defect detection. These
 annotations can used to automatically check that methods are working as
 expected.

Package: libjsr305-java-doc
Architecture: all
Section: doc
Depends: ${java:Depends},
         ${misc:Depends}
Recommends: ${java:Recommends}
Suggests: libjsr305-java
Description: Java library that provides annotations for software defect detection (API)
 This library provides the implementation of Java Specification Request 305.
 JSR-305 specifies annotations for software defect detection. These
 annotations can used to automatically check that methods are working as
 expected.
 .
 This package contains the API documentation.
